#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <ros/ros.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float32.h>
#include <sensor_msgs/PointCloud2.h>

#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>

static ros::Publisher radar_points_pub;
static Eigen::Matrix4f rv_transform = Eigen::Matrix4f::Identity();
static double _tf_x, _tf_y, _tf_z, _tf_roll, _tf_pitch, _tf_yaw;
static Eigen::Matrix4f tf_btol, tf_ltob;

static void points_callback(const sensor_msgs::PointCloud2::ConstPtr& input)
{
     pcl::PointCloud<pcl::PointXYZI> tmp;
     pcl::fromROSMsg(*input, tmp);

     pcl::PointCloud<pcl::PointXYZI>::Ptr tmp_ptr(new pcl::PointCloud<pcl::PointXYZI>(tmp));
     pcl::PointCloud<pcl::PointXYZI>::Ptr tranformed_cloud(new pcl::PointCloud<pcl::PointXYZI>());
     pcl::transformPointCloud(*tmp_ptr, *tranformed_cloud, tf_btol);

     sensor_msgs::PointCloud2::Ptr radar_msg_ptr(new sensor_msgs::PointCloud2);

     pcl::toROSMsg(*tranformed_cloud, *radar_msg_ptr);
     radar_msg_ptr->header.frame_id = "velodyne";
     radar_msg_ptr->header.stamp = input->header.stamp;

     radar_points_pub.publish(*radar_msg_ptr);
}

int main(int argc, char** argv)
{

  //transfromation from radar frame to velodyne frame  
  _tf_x = 2.2506;
  _tf_y = 0;
  _tf_z = -1.27;
  _tf_yaw = 0;
  _tf_roll = 0;
  _tf_pitch = 0;

  Eigen::Translation3f tl_btol(_tf_x, _tf_y, _tf_z);                 // tl: translation
  Eigen::AngleAxisf rot_x_btol(_tf_roll, Eigen::Vector3f::UnitX());  // rot: rotation
  Eigen::AngleAxisf rot_y_btol(_tf_pitch, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf rot_z_btol(_tf_yaw, Eigen::Vector3f::UnitZ());
  tf_btol = (tl_btol * rot_z_btol * rot_y_btol * rot_x_btol).matrix();

  Eigen::Translation3f tl_ltob((-1.0) * _tf_x, (-1.0) * _tf_y, (-1.0) * _tf_z);  // tl: translation
  Eigen::AngleAxisf rot_x_ltob((-1.0) * _tf_roll, Eigen::Vector3f::UnitX());     // rot: rotation
  Eigen::AngleAxisf rot_y_ltob((-1.0) * _tf_pitch, Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf rot_z_ltob((-1.0) * _tf_yaw, Eigen::Vector3f::UnitZ());
  tf_ltob = (tl_ltob * rot_z_ltob * rot_y_ltob * rot_x_ltob).matrix();

  ros::init(argc, argv, "radar_trans");

  ros::NodeHandle nh;
  ros::NodeHandle private_nh("~");

  ros::MultiThreadedSpinner spinner(4);

  radar_points_pub = nh.advertise<sensor_msgs::PointCloud2>("/radar_points", 10);
  ros::Subscriber points_sub = nh.subscribe("radar/points", 10, points_callback);

  spinner.spin();
  return 0;
}
